import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'lab8';
  data = [];
  name = '';
  pubDate = '';
  type = '';
  summary = '';
  hardCoverTotal = 0;

  newBook() {
    this.data.push({'name': this.name, 'pubDate': this.pubDate, 'type': this.type, 'summary': this.summary});
    if (this.type == 'Hard Cover') {
      this.hardCoverTotal++;
    }
  }

  deleteBook(idx) {
    if (this.data[idx].type === 'Hard Cover') {
      this.hardCoverTotal--;
    }
    this.data.splice(idx, 1);
  }

  deleteHardCover() {
    const cleared = [];
    // just create a new array without all Hard Cover books
    for (let i = 0; i < this.data.length; i++) {
      if (this.data[i].type === 'Paper Back') {
        cleared.push(this.data[i]);
      }
    }
    this.hardCoverTotal = 0;
    this.data = cleared;
  }
}
